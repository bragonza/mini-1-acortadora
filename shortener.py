#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp
from urllib.parse import unquote

formulario = """
    <br/><br/>
    <form action="" method = "POST"> 
    <label for="url">url :</label>
    <input type="text" name="url" value=""><br>
    <label for="short">short :</label>
    <input type="text" name="short" value="">
    <input type="submit" value="Enviar">
    </form>
"""


class contentApp(webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    urls = {}

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = unquote(request.split('\r\n\r\n', 1)[1])

        return metodo, recurso, cuerpo

    def process(self, petition):

        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        metodo, recurso, cuerpo = petition
        if metodo == "GET":
            if recurso == "/":
                httpCode = "200 OK"
                htmlBody = "<html><body>pagina para recortar URL's <br> urls disponibles: " \
                           + str(self.urls)
            elif recurso in self.urls:
                httpCode = "308 Permanent Redirect"
                htmlBody = '<meta http-equiv="refresh" content="1;url=' + self.urls[recurso] + '">'

            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>No hay disponible redireccionamiento. Utilice el recurso '/'"

        else:
            url = unquote(cuerpo.split("&")[0].split("=")[1])
            short = "/" + cuerpo.split("&")[1].split("=")[1]
            if url != "" and short != "/" :
                if url.startswith('http://') or url.startswith('https://'):
                    self.urls[short] = url
                    httpCode = "200 OK"
                    htmlBody = "<html><body>url original: " \
                               + '<a href="'+ self.urls[short] +' ">"'+ self.urls[short] +'"</a><br>' \
                            + "url recortada: "'<a href="' + self.urls[short] + ' ">"' + short + '"</a>'
                else:
                    url = "https://" + url
                    self.urls[short] = url
                    httpCode = "200 OK"
                    htmlBody = "<html><body>url original: " \
                               + '<a href="'+ self.urls[short] +' ">"'+ self.urls[short] +'"</a><br>' \
                            + "url recortada: " + '<a href="' + self.urls[short] + ' ">"' + short + '"</a>'

            else:
                httpCode = "200 OK"lwls
                htmlBody = "<html><body>404 not found, por favor rellene todos los campos <br>"

        htmlBody += "<br> Formulario: <br>" + formulario + "</body></html>"

        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
